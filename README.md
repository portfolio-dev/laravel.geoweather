##  Weather

Сервис проверки погоды 

### О проекте #

##### Технологии

* Laravel 7
* VueJS, Vuetify
* redis
* [API погоды](https://openweathermap.org/api)

##### Описание
Сервис состоит из одной страницы. При заходе на страницу определяется геолокация клиента и автоматически подставляется город в соответствующее текстовое поле “Местоположение”. 

Возможно вручную указать другой город и по кнопке “Проверить” - информация о погоде обновляется.

В данном примере используется загрузка городов из json-файла - Geographer_RU.json.
При наборе названия города появляется подсказка в виде выпадающего списка.
p.s. файл не самый новый, поэтому если в подсказке проверяемый город не появился, API погоды все равно по нему поиск выполнит.

В `API погоды` вызывается api `http://api.openweathermap.org/data/2.5/forecast
С помощью запроса выводится информация о погоде за 5 дней начиная с текущего дня.
За каждый день выводится средняя дневная температура и обозначение солнечного дня.


### Установка
1. Клон  проекта **laravel.geoweather.git**: `git clone https://bitbucket.org/businesstep/laravel.geoweather.git`
2. Перейти в проект: `cp laravel.geoweather`
3. Файл .env.example скопировать и переименовать в .env. 
В нем указать api key OPRN_WEATHER_APPID, и ip CLIENT_IP (если запуск с localhost)

### Запуск 
 
	1. Установить Composer зависимости: `php composer install`
	2. Сгенерировать ключ: `php artisan key:generate`
	3. Скомпилировать проект: npm run dev
	4. Запустить сервер: `php artisan serve --port=8000`
	
- ####  проверка api
    С помощью Postman проверить работу API

    headers
	
	`Content-Type: application/json`
	
	`Accept: application/json`
	
1. Информация по гео-локации `/api/geo/location` - GET
2. Информация о погоде по конкретному городу `/api/openweather/forecast` - GET
	
	<small>параметры</small>
    
    `city` string название города
	
	`days` int кол-вол дней (на деле кол-во записей, которое возращает api погоды)

	<small>Пример</small>
        `/api/openweather/forecast?city=Санкт-Петербург&days=40`

	
3. Список всех городов(из json-файла) `/geographer/cities` - GET
	
4. Подсказка - список городов 
  `/api/geographer/cities/search?city` - GET
    
	<small>параметры</small>
	
    `city` string название города

    <small>Пример</small>
    `/api/geographer/cities/search?city=Но`

### Скрин

![Alt text](public/screen.png?raw=true "сервис погоды")

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API'], function() {
    Route::get('/geo/location', 'GeoLocationController@location');
    //Route::get('/openweather/forecast', 'OpenWeatherController@forecast');
    Route::get('/openweather/forecast/{city}/{days}', 'OpenWeatherController@forecast');
    Route::get('/geographer/cities', 'GeographerController@cities');
    Route::get('/geographer/cities/search', 'GeographerController@search');
});

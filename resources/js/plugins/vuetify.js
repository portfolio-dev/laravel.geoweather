import Vue from 'vue'
import Vuetify, {
    VApp,
    VContainer,
    VMain,
    VForm,
    VRow,
    VCol,
    VBtn,
    VTextField,
    VIcon,
    VSimpleTable
} from "vuetify/lib";

Vue.use(Vuetify, {
    components: {VApp, VContainer, VMain, VForm, VRow, VCol, VBtn, VTextField, VIcon, VSimpleTable}
})

const opts = {}

export default new Vuetify(opts)

import HomeComponent from '@/js/components/Home/Index';

const routes = [
    {
        name: 'home',
        path: '/',
        component: HomeComponent
    }
];

export default routes;

<?php

declare(strict_types=1);

namespace App\Service;

class OpenWeatherService
{
    /** @var string $url  */
    private $url;

    /** @var  string $city */
    private $city;

    /** @var string $appid  */
    private $appid;

    /** @var string $code */
    private $code;

    public function __construct()
    {
        $this->url = env('OPEN_WEATHER_URL');
        $this->appid = env('OPEN_WEATHER_APPID');
        $this->code = env('OPEN_WEATHER_CODE');
    }

    /**
     * @param array $params
     * @return string
     */
    public function buildUrl(array $params): string
    {
        return $this->url . '?' . http_build_query($params);
    }

    /**
     * @param string $city
     * @param int $days
     * @return array
     */
    public function getForecast(string $city, int $days)
    {
        $fields = [
            'q' => $city.','.$this->code,
            'cnt' => $days,
            'lang' => 'ru',
            'units' => 'metric',
            'appid' => $this->appid
        ];

        /** @var  string $url */
        $url = $this->buildUrl($fields);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_POST, 0);

        $response = curl_exec($ch);
        $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return [$http, $response];
    }

    /**
     * @param array $content
     * @return array
     */
    private function getFormatForecast(array $forecast)
    {
        /** @var array $result */
        $result = [];

        /** @var array $lists */
        $lists = json_decode($forecast[1], true);

        foreach ($lists['list'] as $item) {
            $result[$item['dt_txt']]['temp'] = $item['main']['temp'];
            $result[$item['dt_txt']]['temp_min'] = $item['main']['temp_min'];
            $result[$item['dt_txt']]['temp_max'] = $item['main']['temp_max'];
            $result[$item['dt_txt']]['weather_code'] = $item['weather'][0]['main'];//Clear
            $result[$item['dt_txt']]['weather_description'] = $item['weather'][0]['description'];
            $result[$item['dt_txt']]['weather_icon'] = $item['weather'][0]['icon'];
            $result[$item['dt_txt']]['wind_speed'] = $item['wind']['speed'];
        }

        return $result;
    }

    /**
     * @param array $forecast
     * @return array
     */
    private function groupByDateForecast(array $forecast)
    {
        /** @var array $results */
        $results = [];

        foreach ($forecast as $key => $value) {
            $results[date('Y-m-d', strtotime($key))]['date'] = date('Y-m-d', strtotime($key));
            $results[date('Y-m-d', strtotime($key))]['temp'][date('H:i', strtotime($key))] = $value['temp'];
            $results[date('Y-m-d', strtotime($key))]['weather_code'][date('H:i', strtotime($key))] = $value['weather_code'];
        }

        return $results;
    }

    /**
     * @param string $city
     * @param int $days
     * @return array|null
     */
    public function getAverageTemperature(string $city, int $days)
    {
        /** @var  array $forecast */
        $forecast = $this->getForecast($city, $days);

        if ($forecast[0] !== 200) {
            return null;
        }

        /** @var array $formatForecast */
        $formatForecast = $this->getFormatForecast($forecast);

        if (empty($formatForecast)) {
            return null;
        }

        /** @var array $groupForecast */
        $groupForecast = $this->groupByDateForecast($formatForecast);

        if (empty($groupForecast)) {
            return null;
        }

        $results = array_map(
            function ($item) {
                $data = [
                    'date' => $item['date'],
                    'average' => round(array_sum($item['temp']) / count($item['temp'])),
                    'clear' => in_array('Clear', $item['weather_code']),
                ];

                return $data;
            },
            $groupForecast
        );

        return [$results] ?? null;
    }
}

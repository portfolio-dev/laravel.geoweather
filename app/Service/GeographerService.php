<?php

declare(strict_types=1);

namespace App\Service;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;

class GeographerService
{
    /**
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function getCities()
    {
        // TODO: убрать, если использоваться будет Redis
        /** @var  json $cities */
        $cities = Cache::get('cities');

        if ($cities === null) {
            $cities = Storage::disk('local')->get('Geographer_RU.json');

            Cache::put('cities', $cities, 3600);
        }

        return json_decode($cities, true);

        // TODO: для Redis
        /*
        if ($cities = Redis::get('cities')) {
            return json_decode($cities, true);
        }

        /** @var  json $cities * /
        $cities = Storage::disk('local')->get('Geographer_RU.json');

        Redis::set('cities', $cities);

        return json_decode($cities, true);
        */
    }

    /**
     * @param string $query
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function searchCities(string $query)
    {
        /** @var  array $cities */
        $cities = $this->getCities();

        /** @var  array $results */
        $results = array_map(
            function ($city) use ($query) {
                $name = $city['long']['default'];

                if (stripos($name, $query) !== false) {
                    return $name;
                }
            },
            $cities
        );

        // исключаем null, ''  Значения из массива
        return array_filter($results, 'strlen');
    }
}

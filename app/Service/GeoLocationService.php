<?php

declare(strict_types=1);

namespace App\Service;

class GeoLocationService
{
    /** @var string $ip */
    private $ip;

    public function __construct()
    {
        $this->ip = geoip()->getClientIP();
    }

    /**
     * @return \Torann\GeoIP\Location
     */
    public function getLocation()
    {
        if ('127.0.0.0' === $this->ip) {
            $this->ip = env('CLIENT_IP');
        }

        /** @var  \Torann\GeoIP\Location $location */
        $location = geoip()->getLocation($this->ip);

        return $location['attributes'];
    }
}

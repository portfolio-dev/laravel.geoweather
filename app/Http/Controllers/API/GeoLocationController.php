<?php

declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Service\GeoLocationService;
use Illuminate\Http\Response;

class GeoLocationController extends Controller
{
    /** @var GeoLocationService $geoLocation  */
    private $geoLocation;

    public function __construct(GeoLocationService $geoLocation)
    {
        $this->middleware('guest');
        $this->geoLocation = $geoLocation;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function location()
    {
        /** @var  \Torann\GeoIP\Location $location */
        $location = $this->geoLocation->getLocation();

        return response()->json([
            'error' => false,
            'location' => $location,
        ], Response::HTTP_OK);
    }
}

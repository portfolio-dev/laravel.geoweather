<?php

declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Service\GeographerService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GeographerController extends Controller
{
    /** @var GeographerService $geographer  */
    private $geographer;

    /**
     * @param GeographerService $geoHelper
     */
    public function __construct(GeographerService $geographer)
    {
        $this->geographer = $geographer;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function cities()
    {
        /** @var  array $cities */
        $cities = $this->geographer->getCities();

        return response()->json([
            'error' => false,
            'cities' => $cities,
        ], Response::HTTP_OK);
    }

    public function search(Request $request)
    {
        /** @var array $cities */
        $cities = $this->geographer->searchCities($request->city);

        return response()->json([
            'error' => false,
            'cities' => $cities,
        ], Response::HTTP_OK);
    }
}

<?php

declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\OpenWeatherRequest;
use App\Http\Traits\OpenWeatherTrait;
use App\Service\OpenWeatherService;
use Illuminate\Http\Response;

class OpenWeatherController extends Controller
{
    use OpenWeatherTrait;

    /** @var OpenWeatherService $openweather */
    private $openweather;

    /**
     * @param OpenWeatherService $openweather
     */
    public function __construct(OpenWeatherService $openweather)
    {
        $this->openweather = $openweather;
        $this->middleware('guest');
    }

    /**
     * @param OpenWeatherRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    //public function forecast(OpenWeatherRequest $request)
    public function forecast(string $city, int $days)
    {
        /** @var  string $city */
        //$city = $request->city;

        /** @var  int $days */
        //$days = (int)$request->days;

        $averageTemperature =  $this->openweather->getAverageTemperature($city, $days);

        if (is_null($averageTemperature)) {
            return $this->notFound();
        }

        return response()->json([
            'error' => false,
            'weather' => $averageTemperature,
        ], Response::HTTP_OK);
    }
}
